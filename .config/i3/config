# ArchLinux i3gaps config
# author: UdbhavU
# System: SkyNet-1
# Keyboard: 60%

# Startup---------------------------------------------------------------------------------------------------------------------------
exec_always --no-startup-id xrdb -load ~/.Xresources
exec --no-startup-id "alacritty"
exec --no-startup-id bash ~/.config/i3/scripts/wallpaper.sh
exec --no-startup-id picom --config ~/.config/picom.conf
exec --no-startup-id xfsettingsd
exec --no-startup-id dunst -config ~/.config/dunst/dunstrc
exec_always --no-startup-id ~/.config/polybar/launch.sh
# run with reload


# General------------------------------------------------------------------------------------------------------------------------

# border & title
for_window [class="^.*"] border pixel 2, title_format "<b> %class >> %title </b>"
# gaps
gaps inner 8
gaps outer 0
# font
font pango:SauceCodePro Nerd Font Mono 11
# border colors
client.focused          #ff5555 #ff5555 #F8F8F2 #ff5555   #ff5555
client.focused_inactive #44475A #44475A #F8F8F2 #44475A   #44475A
client.unfocused        #282A36 #282A36 #BFBFBF #282A36   #282A36
client.urgent           #44475A #FF5555 #FF5555 #FF5555   #FF5555 
client.placeholder      #282A36 #282A36 #F8F8F2 #282A36   #282A36
client.background       #F8F8F2


# Variables------------------------------------------------------------------------------------------------------------------------

# Mod1 = Alt
# Mod4 = Super
# mod key used for most binds
set $Mod Mod4           
# direction keys
set $up k
set $down j
set $left h
set $right l
set $rofi_theme_main "~/.local/share/rofi/themes/c64.rasi"
set $rofi_theme_power "~/.local/share/rofi/themes/purple.rasi"
set $rofi_theme_utility "~/.local/share/rofi/themes/Arc-Dark.rasi"
# define names for workspaces
set $ws1    "  Terminal"
set $ws2    "  Code "
set $ws3    "  Internet "
set $ws4    "  Files "
set $ws5    "  Documents "
set $ws6    "  Media "
set $ws7    "  Design "
set $ws8    "  Chat "
set $ws9    "  General "
set $ws10   "  Settings "


# Status Bar------------------------------------------------------------------------------------------------------------------------

# bar {
#         status_command          i3status
#         position                top
#         workspace_buttons       yes
#         font pango:BlexMono Nerd Font Mono 10
#         colors {
#                     background         #282A36
#                     statusline         #F8F8F2
#                     separator          #44475A
#                     focused_workspace  #44475A #44475A #F8F8F2
#                     active_workspace   #282A36 #44475A #F8F8F2
#                     inactive_workspace #282A36 #282A36 #BFBFBF
#                     urgent_workspace   #FF5555 #FF5555 #F8F8F2
#                     binding_mode       #FF5555 #FF5555 #F8F8F2
#         }
# }
#
# bar {
#     exec_always --no-startup-id ~/.dotfiles/.config/polybar/launch.sh
# }

# Bindings------------------------------------------------------------------------------------------------------------------------ 

# Use mouse+mod to move the window 
floating_modifier $mod      
bindsym $mod+d exec rofi -show drun -columns 3
bindsym $mod+x exec --no-startup-id ~/.config/rofi/powermenu/powermenu.sh
bindsym Control+space exec --no-startup-id ~/.config/rofi/launchers/text/launcher.sh
# bindsym Control+space exec --no-startup-id rofi -show run -theme $rofi_theme_utility

bindsym Control+F4 exec --no-startup-id ~/script/rofi-scripts/monitor_layout.sh
bindsym $mod+F1 exec --no-startup-id ~/.config/rofi/launchers/slate/launcher.sh


# kill focused window
bindsym $Mod+Shift+q kill
bindsym Mod1+q kill
bindsym Mod1+F4 kill





# Enter fullscreen mode for the focused container
bindsym Mod1+f fullscreen toggle

# launchers

# core
bindsym $Mod+w          exec --no-startup-id exec firefox
# bindsym $Mod+f          exec --no-startup-id exec rofi -modi "finder:~/script/rofi-scripts/rofi-finder/finder.sh" -show finder  
bindsym $Mod+f          exec --no-startup-id exec alacritty --command joshuto
bindsym $Mod+Shift+f    exec --no-startup-id exec xfe 
bindsym $Mod+Return     exec --no-startup-id exec alacritty
bindsym $Mod+Shift+x          exec betterlockscreen -l dimblur



# Screenshot
bindsym Print exec --no-startup-id "scrot '%S.png' -e 'mv $f $$(xdg-user-dir PICTURES)/Arch-%S-$wx$h.png ; feh $$(xdg-user-dir PICTURES)/Arch-%S-$wx$h.png'"

# audio
bindsym XF86AudioPlay        exec --no-startup-id playerctl play-pause
bindsym XF86AudioNext        exec --no-startup-id playerctl next
bindsym XF86AudioPrev        exec --no-startup-id playerctl previous
bindsym XF86AudioStop        exec --no-startup-id playerctl stop
bindsym XF86AudioMute        exec --no-startup-id pamixer -t
bindsym XF86AudioRaiseVolume exec --no-startup-id pamixer -i 2
bindsym XF86AudioLowerVolume exec --no-startup-id pamixer -d 2

# backlight
bindsym XF86MonBrightnessUp   exec --no-startup-id xbacklight -inc 10
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 10

###########          Workspace Bindings          ###############

# switch to workspace
bindsym $Mod+1 workspace $ws1
bindsym $Mod+2 workspace $ws2
bindsym $Mod+3 workspace $ws3
bindsym $Mod+4 workspace $ws4
bindsym $Mod+5 workspace $ws5
bindsym $Mod+6 workspace $ws6
bindsym $Mod+7 workspace $ws7
bindsym $Mod+8 workspace $ws8
bindsym $Mod+9 workspace $ws9
bindsym $Mod+0 workspace $ws10
bindsym $Mod+Mod1+1 workspace $ws11
bindsym $Mod+Mod1+2 workspace $ws12
bindsym $Mod+Mod1+3 workspace $ws13
bindsym $Mod+Mod1+4 workspace $ws15
bindsym Mod1+shift+1 workspace $ws14
# switch to workspace - numpad alternatives
bindsym $mod+Mod2+KP_1 workspace $ws1
bindsym $mod+Mod2+KP_2 workspace $ws2
bindsym $mod+Mod2+KP_3 workspace $ws3
bindsym $mod+Mod2+KP_4 workspace $ws4
bindsym $mod+Mod2+KP_5 workspace $ws5
bindsym $mod+Mod2+KP_6 workspace $ws6
bindsym $mod+Mod2+KP_7 workspace $ws7
bindsym $mod+Mod2+KP_8 workspace $ws8
bindsym $mod+Mod2+KP_9 workspace $ws9
bindsym $mod+Mod2+KP_0 workspace $ws10

# switch to next or previous workspace
bindsym $mod+Mod1+Left workspace prev
bindsym $mod+Mod1+Right workspace next

# move focused container to workspace
bindsym $Mod+Shift+1 move container to workspace $ws1; workspace $ws1
bindsym $Mod+Shift+2 move container to workspace $ws2; workspace $ws2
bindsym $Mod+Shift+3 move container to workspace $ws3; workspace $ws3
bindsym $Mod+Shift+4 move container to workspace $ws4; workspace $ws4
bindsym $Mod+Shift+5 move container to workspace $ws5; workspace $ws5
bindsym $Mod+Shift+6 move container to workspace $ws6; workspace $ws6
bindsym $Mod+Shift+7 move container to workspace $ws7; workspace $ws7
bindsym $Mod+Shift+8 move container to workspace $ws8; workspace $ws8
bindsym $Mod+Shift+9 move container to workspace $ws9; workspace $ws9
bindsym $Mod+Shift+0 move container to workspace $ws10; workspace $ws10

# move focused container to workspace - numpad alternatives
bindsym $Mod+Shift+Mod2+KP_End   move container to workspace $ws1; workspace $ws1
bindsym $Mod+Shift+Mod2+KP_Down  move container to workspace $ws2; workspace $ws2
bindsym $Mod+Shift+Mod2+KP_Next  move container to workspace $ws3; workspace $ws3
bindsym $Mod+Shift+Mod2+KP_Left  move container to workspace $ws4; workspace $ws4
bindsym $Mod+Shift+Mod2+KP_Begin move container to workspace $ws5; workspace $ws5
bindsym $Mod+Shift+Mod2+KP_Right move container to workspace $ws6; workspace $ws7
bindsym $Mod+Shift+Mod2+KP_Home  move container to workspace $ws7; workspace $ws7
bindsym $Mod+Shift+Mod2+KP_Up    move container to workspace $ws8; workspace $ws8

mode "resize" 
{
    # Resize with the arrow keys
    bindsym $left  resize shrink width  1 px or 1 ppt
    bindsym $down  resize grow   height 1 px or 1 ppt
    bindsym $up    resize shrink height 1 px or 1 ppt
    bindsym $right resize grow   width  1 px or 1 ppt

    # Back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"
##############     Reload configs           ################

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $Mod+Shift+r restart

# reload the configuration file
bindsym $Mod+Shift+c reload

############      Container/Window control  ############

# Scratchpad, Floating
bindsym $Mod+space floating toggle


bindsym $Mod+Shift+z move scratchpad
bindsym $Mod+z scratchpad show

# change focus
bindsym $Mod+$left  focus left
bindsym $Mod+$down  focus down
bindsym $Mod+$up    focus up
bindsym $Mod+$right focus right

# alternatively, you can use the cursor keys:
bindsym $Mod+Left  focus left
bindsym $Mod+Down  focus down
bindsym $Mod+Up    focus up
bindsym $Mod+Right focus right

bindsym $Mod+p focus parent
bindsym $Mod+c focus child

# move focused window
bindsym $Mod+Shift+$left  move left  10px
bindsym $Mod+Shift+$down  move down  10px
bindsym $Mod+Shift+$up    move up    10px
bindsym $Mod+Shift+$right move right 10px

# alternatively, you can use the cursor keys:
bindsym $Mod+Shift+Up    move up    10px
bindsym $Mod+Shift+Down  move down  10px
bindsym $Mod+Shift+Left  move left  10px
bindsym $Mod+Shift+Right move right 10px

# Size
bindsym Mod1+Up    resize shrink height 10 px or 1 ppt
bindsym Mod1+Down  resize grow   height 10 px or 1 ppt
bindsym Mod1+Left  resize shrink width  10 px or 1 ppt
bindsym Mod1+Right resize grow   width  10 px or 1 ppt

# layout toggle, keycode 23 is Tab
#bindcode Mod1+23 layout toggle tabbed split
#bindcode $Mod+23 layout toggle splitv splith

# switch to workspace with urgent window
for_window [urgent="latest"] focus
focus_on_window_activation   focus

# container layout
bindsym $Mod+Shift+t layout tabbed
bindsym $Mod+Shift+s layout stacking
bindsym $Mod+e layout toggle split


bindsym $mod+Tab workspace next
bindsym $mod+Shift+Tab workspace prev

default_orientation horizontal

################ monitor ##################

bindsym $mod+m move workspace to output right 
###############      Border & Gaps     ###############

new_window normal
new_float  normal

hide_edge_borders both

bindsym $Mod+shift+b border toggle

# changing border style
bindsym $Mod+n border normal
bindsym $Mod+y border 1pixel
bindsym $Mod+u border none

# change gaps
bindsym $Mod+plus                gaps inner current plus  5
bindsym $Mod+minus               gaps inner current minus 5
bindsym $Mod+Shift+plus          gaps outer current plus  5
bindsym $Mod+Shift+minus         gaps outer current minus 5
bindsym $Mod+Control+plus        gaps inner all     plus  5
bindsym $Mod+Control+minus       gaps inner all     minus 5
bindsym $Mod+Control+Shift+plus  gaps outer all     plus  5
bindsym $Mod+Control+Shift+minus gaps outer all     minus 5

############    application settings   ############

# assign applications to specific workspace

assign [class="(?i)(?:alacritty)"] → $ws1
assign [class="(?i)(?:code)"]   → $ws2
assign [class="(?i)(?:firefox)"] → $ws3
assign [class="(?i)(?:discord)"]  → $ws8
assign [class="(?i)(?:notion)"]  → $ws9
assign [class="(?i)(?:font-manager)"]  → $ws10
assign [class="(?i)(?:xfce4-settings-manager)"]  → $ws10
assign [class="(?i)(?:stacer)"]  → $ws10
assign [class="(?i)(?:kitty)"] → $ws1
assign [class="(?i)(?:spotify)"] → $ws6
# for_window [class="Alacritty"] move scratchpad, move position 150 150, resize set 1700 800;

# focus, floating, & sticky
for_window [class="(?i)(?:qt5ct|pinentry)"]          floating enable, focus
for_window [title="(?i)(?:copying|deleting|moving)"] floating enable
for_window [window_role="(?i)(?:pop-up|setup)"]      floating enable

popup_during_fullscreen smart


